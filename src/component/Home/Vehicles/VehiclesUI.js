import React from 'react'
// import { NavLink } from 'react-router-dom';
import './Vehicle.css';

const VehiclesUI = (props) => {
    return (
        <div>
            <div className="container ma_tb">
                <div className="card vehicle_card text-center">
                    <div className="overflow">
                        <img src={props.vehicleImg} alt="vehicleImg" className="vehicle_img" />
                    </div>
                    <div className="card-body text-dark">
                        <h4 className="card-title">
                            {props.vehicleName}
                        </h4>
                        <p className="card-text text-secondary">
                           <span>{props.vehiclePlace}</span> {props.vehiclePrice}
                        </p>
                        {/* <NavLink to="/buses">Explore More</NavLink> */}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default VehiclesUI
