import React from "react";

const Popup = (props) => {
  return props.trigger ? (
    <div className="popup">
      <div className="popup_inner">
        <button className="close_button btn btn-primary" onClick= {()=>{
            props.setTrigger(false)
        }}>
            Close
        </button>
        {props.children}
      </div>
    </div>
  ) : (
    ""
  );
};

export default Popup;
