import React,{useState} from "react";
import Popup from "./Popup";
import LoginUI from "./LoginUI";
import './Login.css';

const Login = () => {
    const [trigger, setTrigger] = useState(false)
  return (
    <div>
      <button className="btn btn-outline-secondary header_login_btn" onClick = {() => {
        setTrigger(true)
      }
      }>
        Login
      </button>
         <Popup trigger={trigger} setTrigger = {setTrigger} >
            <LoginUI />
         </Popup>
    </div>
  );
};

export default Login;
