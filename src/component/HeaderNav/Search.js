import React from "react";
import { Form , FormControl, Button} from "react-bootstrap";

const Search = () => {
  return (
    <div>
      <Form className="d-flex p-2">
        <FormControl
          type="search"
          placeholder="Search Places (e.g: Pokhara)"
          className="mr-2"
          aria-label="Search"
        />
        <Button variant="outline-secondary">Search</Button>
      </Form>
    </div>
  );
};

export default Search;
